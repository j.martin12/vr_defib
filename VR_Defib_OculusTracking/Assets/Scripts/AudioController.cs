using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Columbia
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioController : MonoBehaviour
    {
        // Private
        private AudioSource audioSource = null;
        private AudioClip previousAudioClip = null;
        
        // Serialized
        [Tooltip("Audio clip arrays with a value for playing the clip based on the array value.")]
        [SerializeField]
        private AudioClip[] audioClips;
        [Tooltip("Volume set here will override the volume set on the attached sound source component.")]
        [Range(0f, 1f)]
        [SerializeField]
        private float volume = 0.7f;
        [Tooltip("Pitch set here will override the volume set on the attached sound source component.")]
        [SerializeField]
        [Range(-3f,3f)]
        [Space(10)]
        private float pitch = 1f;
        [Tooltip("True by default. Set to false for sounds to bypass the spatializer plugin. Will override settings on attached audio source.")]
        [SerializeField]
        [Space(10)]
        private bool spatialize = true;
        [Tooltip("False by default. Set to true to enable looping on this sound. Will override settings on attached audio source.")]
        [SerializeField]
        private bool loop = false;
        
        protected virtual void Start()
        {
            audioSource = gameObject.GetComponent<AudioSource>();
            // Validate that we have audio to play
            Assert.IsTrue(audioClips.Length > 0, "An AudioTrigger instance in the scene has no audio clips.");
            // Copy over values from the audio trigger to the audio source
            audioSource.volume = volume;
            audioSource.pitch = pitch;
            audioSource.spatialize = spatialize;
            audioSource.loop = loop;
        }

        public void PlayAudio(int number)
        {
            audioSource.clip = audioClips[number];
            audioSource.Play();
        }

        public void PauseAudion(int number)
        {
            audioSource.clip = audioClips[number];
            audioSource.Pause();
        }
        public void StopAudion(int number)
        {
            audioSource.clip = audioClips[number];
            audioSource.Stop();
        }
        public void UnPauseAudio(int number)
        {
            audioSource.clip = audioClips[number];
            audioSource.UnPause();
        }
    }
}

