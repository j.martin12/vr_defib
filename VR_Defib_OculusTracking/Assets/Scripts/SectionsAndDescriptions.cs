using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Columbia
{
    public class SectionsAndDescriptions : MonoBehaviour
    {
        [SerializeField]
        private Text content = null;
        [SerializeField]
        private TextMesh contentMode = null;
        [SerializeField]
        private static int currentSection;

        public static int CurrentSection
        {
            get => currentSection;
            set => currentSection = value;
        }

        private int totalSectionNumbers;
        [Tooltip("The First Section>Description always needs to describe the Experience, as the Start function will automatically set it.")]
        [SerializeField]
        private List<Section> section;
        [SerializeField]
        private string nextButtonName , prevButtonName, startButtonName;
        private void Start()
        {
            try
            {
                content.text = section[0].actions[0].actionList[0].descriptoin;
                contentMode.text = section[0].sectionName;
                currentSection = 0;
                EventManager.DisableUIButton(prevButtonName);
                totalSectionNumbers = section.Count;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public void DisplaySection(int number)
        {
            content.text = currentSection > 0 && currentSection < totalSectionNumbers 
                ?  section[number].actions[0].actionList[0].descriptoin : "currentSection is Less or More then total Number of Sections";
            currentSection = number;
        }

        public void DisplayNextSection()
        {
            currentSection = currentSection < totalSectionNumbers ?  SwitchTosection(1) : SwitchTosection(0);
        }
        
        public void DisplayPrevSection()
        {
            currentSection = currentSection > -1 ?  SwitchTosection(-1) : SwitchTosection(0);
        }

        public void StartAction(int number)
        {
            content.text = LoadContent(currentSection, number);
        }
        int SwitchTosection(int number)
        {
            if (number != 0)
            {

                if (number != 1 || number != totalSectionNumbers - 2)
                {
                    number += currentSection;
                }
                if(number == 0){
                    EventManager.DisableUIButton(prevButtonName);
                }
                else
                {
                    EventManager.EnableUIButton(prevButtonName);
                }
                if (number == totalSectionNumbers -1)
                {
                    EventManager.DisableUIButton(nextButtonName);
                }
                else
                {
                    EventManager.EnableUIButton(nextButtonName);
                }
                contentMode.text = section[number].sectionName;
                content.text = section[number].actions[0].actionList[0].descriptoin;
                
            }
            else
            {
                content.text = "The request number is larger or smaller then the List Description size!!!!!!";
            }
            return number;
        }

        string LoadContent(int sectionNumber, int actionNuber)
        {
            string combineText = null;
            foreach (var VARIABLE in section[sectionNumber].actions[actionNuber].actionList)
            {
                combineText += VARIABLE.actionName + " " + VARIABLE.descriptoin + "\n";
            }
            return combineText;
        }
    }
    
    [System.Serializable]
    public class Section
    {
        public string sectionName;
        public ActionList[] actions;
    }
    [System.Serializable]
    public class ActionList
    {
        public string actionName;
        public ActionDescription[] actionList;
    }
    [System.Serializable]
    public class ActionDescription
    {
        public string actionName;
        [TextArea(1, 15)] public string descriptoin;
    }
    
}