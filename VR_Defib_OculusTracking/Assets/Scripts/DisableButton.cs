using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Columbia
{
    public class DisableButton : MonoBehaviour
    {
        public string buttonName;
        private void OnEnable()
        {
            EventManager.DisableNavigation += DisableNavButton;
            EventManager.EnableNavigation += EnableNavButton;
        }

        private void DisableNavButton(string navName)
        {
            
            if (navName == buttonName)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    var child = transform.GetChild(i).gameObject;
                    if (child != null)
                        child.SetActive(false);
                }
            }
        }

        private void EnableNavButton(string navName)
        {
            
            if (navName == buttonName)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    var child = transform.GetChild(i).gameObject;
                    if (child != null)
                        child.SetActive(true);
                }
            }
        }

        private void OnDisable()
        {
            EventManager.DisableNavigation -= DisableNavButton;
            EventManager.EnableNavigation -= EnableNavButton;
        }
    }
}

