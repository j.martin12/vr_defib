using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Columbia
{
    public class ObjectBlinking : MonoBehaviour
    {
        public Material blinkMaterial;
        [SerializeField] private MeshRenderer[] defaultMaterials;
        private MeshRenderer[] copyOfDefaultMater = null;
        [SerializeField] private float interval;
        private int lenght = 0;
        private void Start()
        {
            try
            {
                if (defaultMaterials != null)
                {
                    copyOfDefaultMater = defaultMaterials;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
        public string buttonName;
        private void OnEnable()
        {
            EventManager.MakeObjBlink += BlinkFor;
        }

        private void BlinkFor(string objName)
        {
            StartCoroutine(BlinkForPeriod(interval));
        }

        private void KeepBlinking()
        {
            StartCoroutine(BlinkForPeriod(interval));
        }
        
        private void StopBlinking(string objName)
        {
            StopAllCoroutines();
        }

        IEnumerator BlinkForPeriod(float time)
        {
            foreach (var mesh in defaultMaterials)
            {
                mesh.material = blinkMaterial;
            }
            yield return new WaitForSecondsRealtime(time);
            defaultMaterials = copyOfDefaultMater;
            KeepBlinking();
        }
        
        private void OnDisable()
        {
            EventManager.MakeObjBlink -= BlinkFor;
        }
        
    }
}