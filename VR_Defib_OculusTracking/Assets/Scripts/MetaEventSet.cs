using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Columbia
{
    [System.Serializable]
    public class MetaEvent
    {
        public string eventName;
        public UnityEvent Event;
    }

    [System.Serializable]
    public class MetaEventSet : MonoBehaviour
    {
        public List<MetaEvent> metaEvents;
        public static int sectionToLunch = 0;

        public void LunchOnStart()
        {
            sectionToLunch = SectionsAndDescriptions.CurrentSection;
            metaEvents[sectionToLunch].Event.Invoke();
        }
    }

}