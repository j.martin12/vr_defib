using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Columbia
{
    public class ChangeSize : MonoBehaviour
    {
        void Start()
        {
            EventManager.ExampleEvent += IncreaseSize;
        }

        // Update is called once per frame
        public void IncreaseSize()
        {
            transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        }
    }
}