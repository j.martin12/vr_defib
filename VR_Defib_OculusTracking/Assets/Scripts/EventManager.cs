using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Unity.VisualScripting;

namespace Columbia
{
    public class EventManager : MonoBehaviour
    {
        public static object metaEvents;
        public static event Action ExampleEvent;
        public static event Action<string> DisableNavigation;
        public static event Action<string> EnableNavigation;
        public static event Action<string> MakeObjBlink;
        public void EventInAction() //example event that can change Obj Size
        {
            ExampleEvent?.Invoke();
        }
        public static void DisableUIButton(string name)
        {
          DisableNavigation?.Invoke(name);
        }
        public static void EnableUIButton(string name)
        {
            EnableNavigation?.Invoke(name);
        }

        public static void MakeItBlink(string name)
        {
            MakeObjBlink?.Invoke(name);
        }
    }
}